package ab1.impl.Dippold;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ab1.TM;

public class TMImpl implements TM {
    //Definition of TM
    private int numStates;
    private Set<Character> alphabet;
    //private int initialState;
    private int haltState;
    private Set<TMTransition> transitons = new HashSet<>();

    //Configuration of TM
    private int actState;
    private String[] leftTapes;
    private char[] readTapes;
    private String[] rightTapes;

    @Override
    public TM reset() {
	    return new TMImpl();
    }

    @Override
    public int getActState() {
	    return actState;
    }

    @Override
    public TM setNumberOfTapes(int numTapes) throws IllegalArgumentException {
        if(numTapes<1) throw new IllegalArgumentException();

        //Initialize tapes
        leftTapes = new String[numTapes];
        readTapes = new char[numTapes];
        rightTapes = new String[numTapes];

        //Set empty tape
        for(int i=0; i<numTapes; i++){
            leftTapes[i] = "";
            readTapes[i] = '#';
            rightTapes[i] = "";
        }
        return this;
    }

    @Override
    public TM setSymbols(Set<Character> symbols) throws IllegalArgumentException {
	    alphabet = symbols;
        return this;
    }

    @Override
    public Set<Character> getSymbols() {
        return alphabet;
    }

    @Override
    public TM addTransition(int fromState, int tapeRead, char symbolRead, int toState, int tapeWrite, char symbolWrite,
	    Movement tapeReadMovement, Movement tapeWriteMovement) throws IllegalArgumentException {
        if(fromState == 0) throw new IllegalArgumentException();
        if(false) throw new IllegalArgumentException(); //Transition nicht eindeutig (fromState, symbolRead)    ????
        if(!this.alphabet.contains(symbolRead)) throw new IllegalArgumentException();
        if(tapeRead < 0 || tapeRead >= readTapes.length) throw new IllegalArgumentException();
        if(tapeWrite < 0 || tapeWrite >= readTapes.length) throw new IllegalArgumentException();

        this.numStates = Math.max(Math.max(fromState, toState), numStates); //Expand states to largest state used

        transitons.add(new TMTransition(fromState, tapeRead, symbolRead, toState, tapeWrite, symbolWrite, tapeReadMovement, tapeWriteMovement));
	    return this;
    }

    @Override
    public int getNumberOfStates() {
        return numStates;
    }

    @Override
    public int getNumberOfTapes() {
        return readTapes.length;
    }

    @Override
    public TM setNumberOfStates(int numStates) throws IllegalArgumentException {
        if(numStates<1) throw new IllegalArgumentException();

        this.numStates = numStates;
	    return this;
    }

    @Override
    public TM setHaltState(int state) throws IllegalArgumentException {
        if(state<0) throw new IllegalArgumentException();
        if(state>numStates) throw new IllegalArgumentException();

        this.haltState = state;
	    return this;
    }

    @Override
    public TM setInitialState(int state) throws IllegalArgumentException {
        if(state<1) throw new IllegalArgumentException();
        if(state>numStates) throw new IllegalArgumentException();

        this.actState = state;
        return this;
    }

    @Override
    public TM setInitialTapeContent(int tape, char[] content) {
        leftTapes[tape] = new String(content);
	    return this;
    }

    @Override
    public TM doNextStep() throws IllegalStateException {
        if(isHalt()) throw new IllegalStateException();

        for(TMTransition transition : transitons){
            if(transition.fromState != this.actState) continue;
            if(transition.symbolRead != this.readTapes[transition.tapeRead]) continue;

            this.readTapes[transition.tapeWrite] = transition.symbolWrite;
            moveTape(transition.tapeRead, transition.tapeReadMovement);
            moveTape(transition.tapeWrite, transition.tapeWriteMovement);

            this.actState = transition.toState;
            return this;
        }
        throw new IllegalStateException();
    }

    @Override
    public boolean isHalt() {
        if(this.actState == this.haltState) return true;
	    else return false;
    }

    @Override
    public boolean isCrashed() {
        if(this.actState == -1) return true;
        else return false;
    }

    @Override
    public List<TMConfig> getTMConfig() {
        if(isCrashed()) return null;

        List<TMConfig> tmConfigList = new ArrayList<>();
        for(int i=0; i<readTapes.length; i++){
            tmConfigList.add(getTMConfig(i));
        }
	    return tmConfigList;
    }

    @Override
    public TMConfig getTMConfig(int tape) {
        return new TMConfig(leftTapes[tape].toCharArray(), readTapes[tape], rightTapes[tape].toCharArray());
    }

    private void moveTape(int tapeId, Movement direction){
        switch (direction){
            case Stay: break;

            case Left:
                if(leftTapes.length == 0) throw new IllegalStateException();

                if(!(this.rightTapes[tapeId].isEmpty() && this.readTapes[tapeId] == '#'))
                    this.rightTapes[tapeId] = this.readTapes[tapeId] + this.rightTapes[tapeId];
                this.readTapes[tapeId] = this.leftTapes[tapeId].charAt(this.leftTapes[tapeId].length()-1);
                this.leftTapes[tapeId] = this.leftTapes[tapeId].substring(0, this.leftTapes[tapeId].length()-1);
                break;

            case Right:
                this.leftTapes[tapeId] = this.leftTapes[tapeId] + this.readTapes[tapeId];
                this.readTapes[tapeId] = this.rightTapes[tapeId].isEmpty() ? '#' : this.rightTapes[tapeId].charAt(0);
                this.rightTapes[tapeId] = this.rightTapes[tapeId].isEmpty() ? "" : this.rightTapes[tapeId].substring(1);
                break;
        }

    }
    private class TMTransition{
        private int fromState;
        private int tapeRead;
        private char symbolRead;
        private int toState;
        private int tapeWrite;
        private char symbolWrite;
        private Movement tapeReadMovement;
        private Movement tapeWriteMovement;

        public TMTransition(int fromState, int tapeRead, char symbolRead, int toState, int tapeWrite, char symbolWrite, Movement tapeReadMovement, Movement tapeWriteMovement) {
            this.fromState = fromState;
            this.tapeRead = tapeRead;
            this.symbolRead = symbolRead;
            this.toState = toState;
            this.tapeWrite = tapeWrite;
            this.symbolWrite = symbolWrite;
            this.tapeReadMovement = tapeReadMovement;
            this.tapeWriteMovement = tapeWriteMovement;
        }

        public int getFromState() {
            return fromState;
        }

        public void setFromState(int fromState) {
            this.fromState = fromState;
        }

        public int getTapeRead() {
            return tapeRead;
        }

        public void setTapeRead(int tapeRead) {
            this.tapeRead = tapeRead;
        }

        public char getSymbolRead() {
            return symbolRead;
        }

        public void setSymbolRead(char symbolRead) {
            this.symbolRead = symbolRead;
        }

        public int getToState() {
            return toState;
        }

        public void setToState(int toState) {
            this.toState = toState;
        }

        public int getTapeWrite() {
            return tapeWrite;
        }

        public void setTapeWrite(int tapeWrite) {
            this.tapeWrite = tapeWrite;
        }

        public char getSymbolWrite() {
            return symbolWrite;
        }

        public void setSymbolWrite(char symbolWrite) {
            this.symbolWrite = symbolWrite;
        }

        public Movement getTapeReadMovement() {
            return tapeReadMovement;
        }

        public void setTapeReadMovement(Movement tapeReadMovement) {
            this.tapeReadMovement = tapeReadMovement;
        }

        public Movement getTapeWriteMovement() {
            return tapeWriteMovement;
        }

        public void setTapeWriteMovement(Movement tapeWriteMovement) {
            this.tapeWriteMovement = tapeWriteMovement;
        }
    }
}
